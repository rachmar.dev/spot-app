import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-viewspot',
  templateUrl: './viewspot.page.html',
  styleUrls: ['./viewspot.page.scss'],
})
export class ViewspotPage implements OnInit {

  public spotDatas : any ; 

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
  ) {
    this.spotDatas = navParams.data;
    console.log(this.spotDatas);
  }

  ngOnInit() {
  }

  closeModal(){
    console.log('close add modal...');
    this.modalController.dismiss();
  }

}
