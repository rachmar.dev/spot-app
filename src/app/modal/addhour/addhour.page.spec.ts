import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddhourPage } from './addhour.page';

describe('AddhourPage', () => {
  let component: AddhourPage;
  let fixture: ComponentFixture<AddhourPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddhourPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddhourPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
