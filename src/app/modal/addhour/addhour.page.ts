import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-addhour',
  templateUrl: './addhour.page.html',
  styleUrls: ['./addhour.page.scss'],
})
export class AddhourPage implements OnInit {

  constructor(
    private modalController: ModalController,
    private navParams: NavParams
  ) {}

  ngOnInit() {
  }

  closeModal(){
    console.log('close add modal...');
    this.modalController.dismiss();
  }

  addHour(form){

    let hourData = {
      "day": form.value.day,
      "start_time": form.value.start,
      "end_time": form.value.end,
      "type": form.value.type
    }
    this.modalController.dismiss(hourData);

  }

}
