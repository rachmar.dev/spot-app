import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddmenuPage } from './addmenu.page';

describe('AddmenuPage', () => {
  let component: AddmenuPage;
  let fixture: ComponentFixture<AddmenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddmenuPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddmenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
