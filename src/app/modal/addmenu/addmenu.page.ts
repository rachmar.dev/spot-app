import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-addmenu',
  templateUrl: './addmenu.page.html',
  styleUrls: ['./addmenu.page.scss'],
})
export class AddmenuPage implements OnInit {

  constructor(
    private modalController: ModalController,
    private navParams: NavParams
  ) {}

  ngOnInit() {
  }

  closeModal(){
    console.log('close add  modal...');
    this.modalController.dismiss();
  }

  addMenu(form){

    let menuData = {
      "name": form.value.name,
      "description": form.value.description,
      "price": {
        "start": form.value.start,
        "end": form.value.end
      }
    }
    this.modalController.dismiss(menuData);

  }
}
