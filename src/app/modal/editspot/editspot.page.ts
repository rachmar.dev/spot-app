import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EdithourPage } from '../edithour/edithour.page';
import { EditmenuPage } from '../editmenu/editmenu.page';
import { AddhourPage } from '../addhour/addhour.page';
import { AddmenuPage } from '../addmenu/addmenu.page';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-editspot',
  templateUrl: './editspot.page.html',
  styleUrls: ['./editspot.page.scss'],
})
export class EditspotPage implements OnInit {

  public spotData : any ; 
  public apiUrl = 'http://localhost:3000';
  public spotID : any;
  public picture : any;
  public base64Image: any;

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private  http:  HttpClient,
    private camera: Camera
  ) {
    this.spotData = navParams.data;
    this.spotID  = navParams.data._id;
    this.picture = navParams.data.picture;
    console.log(this.spotData);
  }


  ngOnInit() {
  }

  async updateSpot(form){

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type' : 'application/json'
      })
    };

    let postData = {
      "name": form.value.name,
      "description": form.value.description,
      "hours":this.spotData.hours,
      "type": form.value.type,
      "menu":this.spotData.menu,
      "picture": this.picture
    }

    try {
      const returnData = await this.http.patch(this.apiUrl+'/spots/'+this.spotID, postData, httpOptions).toPromise();
      this.modalController.dismiss(returnData);
      console.log(returnData);
    } catch(error) {
      console.log(error);
    }

    
  }

  async uploadBase64Image(){

    const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type' : 'application/json'
        })
      };
  
      let postData = {
        "base64": this.base64Image
      }
  
      try {
        const returnData : any = await this.http.post(this.apiUrl+'/upload', postData, httpOptions).toPromise();
        this.picture = this.apiUrl+returnData.link
        console.log();
      } catch(error) {
        console.log(error);
      }

  }

  changeImage() {

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }
    
    this.camera.getPicture(options).then((imageData) => {
     this.base64Image = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log(err);
    });

  }

  async openAddHourModal() {
    console.log('open add hour modal...');
    const modal = await this.modalController.create({component: AddhourPage});
    await modal.present();
    const hourData = await modal.onWillDismiss();
    if (typeof hourData.data !== "undefined") {
      this.spotData.hours.push(hourData.data);
    }
    console.log(this.spotData);
  }

  async openAddMenuModal() {
    console.log('open add menu modal...');
    const modal = await this.modalController.create({component: AddmenuPage});
    await modal.present();
    const menuData = await modal.onWillDismiss();
    if (typeof menuData.data !== "undefined") {
      this.spotData.menu.push(menuData.data);
    }
    console.log(this.spotData);
  }

  async editHour(hour){
    console.log('open add modal...');
    const modal = await this.modalController.create({component: EdithourPage,componentProps:hour});
    await modal.present();
    const hourData = await modal.onWillDismiss();
    if (typeof hourData.data !== "undefined") {
      let index = this.spotData.hours.indexOf(hour);
      this.spotData.hours.splice(index, 1,hourData.data);
    }
    console.log(this.spotData);
  }

  async editMenu(menu){
    console.log('open add modal...');
    const modal = await this.modalController.create({component: EditmenuPage,componentProps:menu});
    await modal.present();
    const menuData = await modal.onWillDismiss();
    if (typeof menuData.data !== "undefined") {
      let index = this.spotData.menu.indexOf(menu);
      this.spotData.menu.splice(index, 1,menuData.data);
    }
  }

  deleteHour(hour){
    let index = this.spotData.hours.indexOf(hour);
    this.spotData.hours.splice(index, 1);
  }

  deleteMenu(menu){
    let index = this.spotData.menu.indexOf(menu);
    this.spotData.menu.splice(index, 1);
  }

  closeModal(){
    console.log('close add modal...');
    this.modalController.dismiss();
  }

}
