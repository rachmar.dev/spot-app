import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditspotPage } from './editspot.page';

describe('EditspotPage', () => {
  let component: EditspotPage;
  let fixture: ComponentFixture<EditspotPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditspotPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditspotPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
