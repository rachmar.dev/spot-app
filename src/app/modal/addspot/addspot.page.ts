import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { AddhourPage } from '../addhour/addhour.page';
import { AddmenuPage } from '../addmenu/addmenu.page';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-addspot',
  templateUrl: './addspot.page.html',
  styleUrls: ['./addspot.page.scss'],
})
export class AddspotPage implements OnInit {
  

  public hours : any[];
  public menus : any[];
  public base64Image: any;
  public picture = "https://i1.wp.com/ssl.gstatic.com/s2/profiles/images/silhouette80.png";
  public apiUrl = 'http://localhost:3000';

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private  http:  HttpClient,
    private camera: Camera
  ) {
    this.hours = [];
    this.menus = [];
  }

  ngOnInit() {
  }

  async openAddHourModal() {
    console.log('open add hour modal...');
    const modal = await this.modalController.create({component: AddhourPage});
    await modal.present();
    const hourData = await modal.onWillDismiss();
    if (typeof hourData.data !== "undefined") {
      this.hours.push(hourData.data);
    }
    console.log(this.hours);
  }

  async openAddMenuModal() {
    console.log('open add menu modal...');
    const modal = await this.modalController.create({component: AddmenuPage});
    await modal.present();
    const menuData = await modal.onWillDismiss();
    if (typeof menuData.data !== "undefined") {
      this.menus.push(menuData.data);
    }
    console.log(this.menus);
  }

  async addSpot(form){

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type' : 'application/json'
      })
    };

    let postData = {
      "name": form.value.name,
      "description": form.value.description,
      "hours":this.hours,
      "type": form.value.type,
      "menu":this.menus,
      "picture": this.picture
    }

    try {
      const returnData = await this.http.post(this.apiUrl+'/spots', postData, httpOptions).toPromise();
      this.modalController.dismiss(returnData);
      console.log(returnData);
    } catch(error) {
      console.log(error);
    }

  }

  async uploadBase64Image(){

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type' : 'application/json'
        })
      };
  
      let postData = {
        "base64": this.base64Image
      }
  
      try {
        const returnData : any = await this.http.post(this.apiUrl+'/upload', postData, httpOptions).toPromise();
        this.picture = this.apiUrl+returnData.link
        console.log();
      } catch(error) {
        console.log(error);
      }

  }

  changeImage() {

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }
    
    this.camera.getPicture(options).then((imageData) => {
     this.base64Image = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log(err);
    });

  }

  deleteHour(hour){
    let index = this.hours.indexOf(hour);
    this.hours.splice(index, 1);

  }

  deleteMenu(menu){
    let index = this.menus.indexOf(menu);
    this.menus.splice(index, 1);

  }

  closeModal(){
    console.log('close add modal...');
    this.modalController.dismiss();
  }
}
