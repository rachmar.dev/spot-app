import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddspotPage } from './addspot.page';

describe('AddspotPage', () => {
  let component: AddspotPage;
  let fixture: ComponentFixture<AddspotPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddspotPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddspotPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
