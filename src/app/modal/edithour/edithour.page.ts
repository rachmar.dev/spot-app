import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-edithour',
  templateUrl: './edithour.page.html',
  styleUrls: ['./edithour.page.scss'],
})
export class EdithourPage implements OnInit {

  public spotData : any ; 

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
  ) {
    this.spotData = navParams.data;
    console.log(this.spotData);
  }
  ngOnInit() {
  }

  editHour(form){

    let newHourData = {
      "day": form.value.day,
      "start_time": form.value.start,
      "end_time": form.value.end,
      "type": form.value.type
    }
    this.modalController.dismiss(newHourData);
  
  }

  closeModal(){
    console.log('close add modal...');
    this.modalController.dismiss();
  }

}
