import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdithourPage } from './edithour.page';

describe('EdithourPage', () => {
  let component: EdithourPage;
  let fixture: ComponentFixture<EdithourPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdithourPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdithourPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
