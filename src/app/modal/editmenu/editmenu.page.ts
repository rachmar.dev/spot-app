import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-editmenu',
  templateUrl: './editmenu.page.html',
  styleUrls: ['./editmenu.page.scss'],
})
export class EditmenuPage implements OnInit {

  public spotData : any ; 

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
  ) {
    this.spotData = navParams.data;
    console.log(this.spotData);
  }
  ngOnInit() {
  }

  editMenu(form){

    let newMenuData = {
      "name": form.value.name,
      "description": form.value.description,
      "price": {
        "start": form.value.start,
        "end": form.value.end
      }
    }
    
    this.modalController.dismiss(newMenuData);
  
  }

  closeModal(){
    console.log('close add modal...');
    this.modalController.dismiss();
  }

}
