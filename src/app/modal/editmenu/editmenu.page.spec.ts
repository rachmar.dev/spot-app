import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditmenuPage } from './editmenu.page';

describe('EditmenuPage', () => {
  let component: EditmenuPage;
  let fixture: ComponentFixture<EditmenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditmenuPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditmenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
