import { Component, OnInit } from '@angular/core';
import { ModalController ,ToastController } from '@ionic/angular';
import { AddspotPage } from '../modal/addspot/addspot.page';
import { EditspotPage } from '../modal/editspot/editspot.page';
import { ViewspotPage } from '../modal/viewspot/viewspot.page';
import { HttpClient, HttpHeaders } from '@angular/common/http'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  public apiUrl = 'http://localhost:3000';

  public spotDatas : any ; 


  constructor(
    private modalController: ModalController,
    private toast : ToastController,
    private http: HttpClient
  ) {
    
    this.getAllSpotsData();

  }

  ngOnInit() {
    console.log('asd');
  }

  async getAllSpotsData() {
    try {
      this.spotDatas = await this.http.get(this.apiUrl+'/spots').toPromise();
      console.log(this.spotDatas);
    } catch(error) {
      console.log(error);
    }
  }

  async deleteSpot(spot : any){
    try {
      const returnData = await this.http.delete(this.apiUrl+'/spots/'+ spot._id).toPromise();
      this.getAllSpotsData();
      this.displayNotification(spot.name + ' was successfully deleted');      
      console.log(returnData);
    } catch(error) {
      console.log(error);
    }
  }

  async viewSpot(spot : any) {
    console.log('open add modal...');
    const modal = await this.modalController.create({
      component: ViewspotPage,
      componentProps: spot
    });
    return await modal.present();
  }


  async openAddModal() {

    console.log('open add modal...');
    const modal = await this.modalController.create({component: AddspotPage});
    await modal.present();
    const menuData = await modal.onWillDismiss();
    if (typeof menuData.data !== "undefined") {
      this.getAllSpotsData();
      this.displayNotification('Spot was successfully added!'); 
    }
    
  }

  async editSpot(spot : any) {

    console.log('open add modal...');
    const modal = await this.modalController.create({
      component: EditspotPage,
      componentProps: spot
    });
    await modal.present();
    const menuData = await modal.onWillDismiss();
    if (typeof menuData.data !== "undefined") {
      this.getAllSpotsData();
      this.displayNotification('Spot was successfully edited!'); 
    }
    
  }
  
  async displayNotification(message : string) {
    const toast = await this.toast.create({
      message: message,
      position: "top",
      duration: 2000
    });
    toast.present();
  }


}
